# NatureFood-19100372

## Julia Installation

If you are not familiar with Julia, probably the easiest way to get up and running on your computer is to follow the [install instructions](http://docs.junolab.org/latest/man/installation/) for [Juno](http://www.junolab.org). Basically, this involves
* installing [Julia](https://julialang.org/downloads/)
* installing [Atom](https://atom.io)
* installing [Juno](http://www.junolab.org), which is an Atom package which provides an integrated development environment (IDE) for Julia.

The scripts have been run successfully using the latest versions of Julia (v1.3.1) and [JuMP](https://github.com/JuliaOpt/JuMP.jl) (v0.20.0) on both MacOS and Windows.

If you are familiar with Julia and do not want to install Juno one option is to run the script by including it from the Julia REPL with the command ```include("DietOptim.jl")```

A third option is to install Jupyter and import the DietOptim.jl script into a Julia Jupyter notebook to run it there.

If you are unfamiliar with the command line the Juno approach is recommended.

## Downloading the script and data files and installing necessary packages

Assuming you have followed the instructions to install Juno, you must first clone this repository by downloading it as a zip or from the command line on MacOS or Linux:
```
git clone https://gitlab.com/thetasolutionsllc/naturefood-19100372.git
```
Then in Juno go File->Add Project Folder and navigate to the naturefood-19100372 folder that was just downloaded.

To install the necessary packages, from the Juno menu select "Open REPL" and a Julia prompt will appear. Install the necessary packages by entering the following commands:
```
using Pkg
Pkg.add("DataFrames")
Pkg.add("JuMP")
Pkg.add("GLPK")
Pkg.add("CSV")
Pkg.add("LinearAlgebra")
```

## Running an analysis

Once Julia is set up with the necessary packages you should be able to open the DietOptim.jl file inside Juno and then from the "Juno" dropdown menu select "Run All".

Running the script for the very first time can take some time as Julia must first precompile the packages that were installed but this only needs to be done once.

Subsequent analyses will run near instantaneously.

The DietOptim.jl file is preset to run "Analysis 1" from the paper. If you navigate to line 28 there is an ```analysisPreset``` variable which can be set to values of 1, 2, 3, 4, and 5 in order to run the equivalent analyses in the paper.

On line 31 the ```costScaleFactor``` variable will modify the prices for dairy and animal protein foods by the factor given, i.e. 1.05 (5% increase), 1.1 (10% increase) as described in the paper. This costScaleFactor is only applied when running with ```analysisPreset = 2```.

## Outputs

For each analysis an associated directory will be created in the folder where the source file DietOptim.jl exists.

* results_foodAmtShadowPrice.csv contains information about the shadow prices that are available
* results_optimDiet.csv contains the information about the foods in the lowest cost diet solution
* results_optimDietSummary.csv contains information about the nutrient requirements and the amount of each nutrient that exists in the lowest cost diet solution

The cost of the diet for each analysis is printed to the Julia REPL. For example for ```analysisPreset = 1``` the output is:
```
Objective value (Cost of optimal diet in $): 1.9782168337007753
```

# Acknowledgments

We gratefully acknowledge the constructive input from the reviewers as well as the developers of the Julia language and associated packages.
